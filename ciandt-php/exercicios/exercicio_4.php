<html>
<head>
	<title>CIANDT - Exercício 4 </title>
	<script type="text/javascript" src="../inc/js/verifica_campo.js"></script>
	<script type="text/javascript" src="../inc/js/somente_numero.js"></script>
	<script type="text/javascript" src="../inc/js/mascara.js"></script>
	<script type="text/javascript" src="../inc/js/email.js"></script>

    <script Language="JavaScript">
	    
	    function validaCadastro(theForm){  
			if (!verificaCampo(theForm.nome,'s','3','150','Nome')){return (false);}		
			if (!verificaCampo(theForm.sobrenome,'s','3','150','Sobrenome')){return (false);}					
			if (!verificaCampo(theForm.email,'s','10','100','Email')){return (false);}
			if (!verificaCampo(theForm.telefone,'s','10','13','Telefone')){return (false);}
			if (!verificaCampo(theForm.login,'s','3','30','Login')){return (false);}
			if (!verificaCampo(theForm.senha,'s','5','15','Senha')){return (false);}
			theForm.submit();
		}

	</script>
</head>
<body>

    <div id="alert">
    <?php if (isset($_POST['result']) && $_POST['result'] == 'erro_email') { ?>
        <div class="boxalertmin error">Problema ao inserir registro! <br /> E-mail já cadastrado no sistema!</div><br>
	<?php } else if (isset($_POST['result']) && $_POST['result'] == 'erro_login') { ?>
        <div class="boxalertmin error">Problema ao inserir registro!<br /> Login já cadastrado no sistema!</div><br>	
	<?php } else if (isset($_POST['result']) && $_POST['result'] == 'ok') { ?>
        <div class="boxalertmin accept">Registro inserido com sucesso!</div><br>
    <?php } ?>
    </div>
    <br />  

    <?php
    	if ( (isset($_POST['result']) && $_POST['result'] == 'erro_email') || (isset($_POST['result']) && $_POST['result'] == 'erro_login') ) {
    		$nome 	 	= $_POST['nome'];
			$sobrenome 	= $_POST['sobrenome'];
			$email 	 	= $_POST['email'];
			$telefone 	= $_POST['telefone'];
			$login	 	= $_POST['login'];
			$senha 	 	= $_POST['senha'];
    	} else {
    		$nome 		= ''; 
    		$sobrenome 	= '';
    		$email 		= '';
    		$telefone 	= '';
    		$login 		= '';
    		$senha 		= '';
    	}
    ?> 

	<form id="ciandt-form" action="/ciandt-php/exercicios/exercicio_4_submit.php" method="post" >

		<label for="nome">Nome: </label>
		<input type="text" name="nome" value="<?= $nome; ?>" /><br /><br />

		<label for="sobrenome">Sobrenome: </label>
		<input type="text" name="sobrenome" value="<?= $sobrenome; ?>" /><br /><br />

		<label for="email">E-mail: </label>
		<input type="text" name="email" onblur="splitemail(this);" value="<?= $email; ?>" /><br /><br />

		<label for="telefone">Telefone: </label>
		<input type="text" name="telefone" maxlength="13" OnKeyPress="digita_num(event); mascaraGenerica(this, '## #####-####')" value="<?= $telefone; ?>" autocomplete="off"/><br /><br />

		<label for="login">Login: </label>
		<input type="text" name="login" value="<?= $login; ?>" autocomplete="off"/><br /><br />

		<label for="senha">Senha: </label>
		<input type="password" name="senha" value="<?= $senha; ?>" /><br /><br />

		<input type="submit" name="Enviar" onclick="return validaCadastro(this.form);">

	</form>
</body>
</html>