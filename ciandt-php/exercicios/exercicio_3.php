<!DOCTYPE html>
<html>
<head>
	<title>CIANDT - Exercício 3</title>
</head>
<body>

	<?php
		/* 
	 	 * Observação: Como não ficou claro pra mim se a leitura das extensões seriam feitas através de um array de valores passados ou teria de ser feita a 
	 	 * partir de um diretório qualquer, implementei a solução na qual informamos o diretório na variável $dir e o código scaneia o mesmo informando as extensões
	 	 * encontradas. Caso exista mais de um arquivo com a mesma extensão, será desconsiderado, contabilizando apenas 1.
	 	 */		
		$dir = '../files';
		
		$files = scandir($dir);
		$extensions = array();

		foreach ($files as $key => $name) {
			
			if ( $name != '.' && $name != '..') {

				$filename = explode('.', $name);

				if ( !empty($filename) ) {

					if ( isset($extensions) && !array_key_exists($filename['1'], $extensions) ) {
						$extensions[$filename['1']] = '.'.$filename['1'];
					}
				}

			}
		}

		asort($extensions);
		
		foreach ($extensions as $key => $value) {
			print $value . '<br />';
		}

	?>

</body>
</html>

