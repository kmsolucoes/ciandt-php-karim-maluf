<!DOCTYPE html>
<html>
<head>
	<title>CIANDT - Exercício 1</title>
</head>

<body>
	<?php
		
		$locations = array( 
			'Brasil' 	=> 'Brasilia',
			'Japão'  	=> 'Tóquio',
			'Canada' 	=> 'Ottawa',
			'Espanha' 	=> 'Madri',
			'Portugal'	=> 'Lisboa',
			'Argentina'	=> 'Buenos Aires'); 

		asort($locations);

		foreach ($locations as $country => $capital) {
			echo 'A capital de <b>' . $country . '</b> &eacute; <b>' . $capital . '</b><br />';
		}

	?>
</body>
</html>
