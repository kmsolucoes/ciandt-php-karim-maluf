<?php
	/*
	 * Observação: Para garantir a exatidão de 50%/50% a cada execução, ao invéz de rand() utilizei uma variável de controle em um arquivo. Desta forma consigo 
	 * garantir que sempre a proporção será de 50/50.
	 */

	function foiMordido() {

		$value = file_get_contents('../files/foiMordido.txt');

		if ( $value == 'TRUE' ) {
			$value = 'FALSE';
			file_put_contents('../files/foiMordido.txt', $value);
		} else {
			$value = 'TRUE';
			file_put_contents('../files/foiMordido.txt', $value);
		}

		return $value;
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>CIANDT - Exercício 2</title>
</head>
<body>
	<form id="foiMordido" action="exercicio_2.php" method="post"> 

		<label for="qtdVezes">Informe a quantidade de execuções: </label>
		<input type="text" name="qtdVezes" value=""> <br /><br />
		<input type="submit" name="inicia" value="Iniciar">
	</form>

	<?php 

		if (isset($_POST['qtdVezes']) && $_POST['qtdVezes'] != '' ) {

			for ($i=0; $i < $_POST['qtdVezes'] ; $i++) { 
				$result = foiMordido();
				if ( $result == 'TRUE' ) {
					echo "Joãozinho mordeu o seu dedo <br />";
				} else {
					echo "Joãozinho NÃO mordeu o seu dedo <br />";
				}
			}

		}
	?>
</body>
</html>