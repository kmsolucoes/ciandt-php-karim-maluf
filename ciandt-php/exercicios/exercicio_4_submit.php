<form name="fVolta" id="fVolta" action="/ciandt-php/exercicios/exercicio_4.php" method="post">
    <input type="hidden" name="result" id="result" value=""> 
    <input type="hidden" name="nome" id="nome" value="<?= $_POST['nome']; ?>">
    <input type="hidden" name="sobrenome" id="sobrenome" value="<?= $_POST['sobrenome']; ?>">
    <input type="hidden" name="email" id="email" value="<?= $_POST['email']; ?>">
    <input type="hidden" name="telefone" id="telefone" value="<?= $_POST['telefone']; ?>">
    <input type="hidden" name="login" id="login" value="<?= $_POST['login']; ?>">
    <input type="hidden" name="senha" id="senha" value="<?= $_POST['senha']; ?>">
</form>

<?php
	
	$values = file_get_contents('../files/registro.txt');
	$values = unserialize($values);
	$erro = '';
	
	foreach ($values as $key => $value) {		
		if ( array_search($_POST['email'], $value) ) {
			$erro = 'email';
		}
		if ( array_search($_POST['login'], $value) ) {
			$erro = 'login';
		}
	}
	
	if ( $erro == '' ) {
		$submitted_info['nome'] 	 = $_POST['nome'];
		$submitted_info['sobrenome'] = $_POST['sobrenome'];
		$submitted_info['email'] 	 = $_POST['email'];
		$submitted_info['telefone']  = $_POST['telefone'];
		$submitted_info['login'] 	 = $_POST['login'];
		$submitted_info['senha'] 	 = md5($_POST['senha']);

		$values[] = ($submitted_info);
		file_put_contents('../files/registro.txt', serialize($values));
	}

	echo "<script language='JavaScript'>";

	switch ($erro) {
		case 'email':
			echo "document.forms['fVolta'].result.value='erro_email';";
			break;
		
		case 'login':
			echo "document.forms['fVolta'].result.value='erro_login';";
			break;
		default:
			echo "document.forms['fVolta'].result.value = 'ok';";
			break;
	}

	echo "document.forms['fVolta'].submit();";
	echo "</script>";
?>
