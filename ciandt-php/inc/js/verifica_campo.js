<!--
// onBlur="return verificaCampo(this,'s','3','20','E-mail');"
// Esta função não se modifica. É padrão para todos os forms.
function verificaCampo(input,campo,vmin,vmax,nome){
	var erro = 1; 
	if (campo == 's'){
		if (input.value == ""){
   			alert("O campo \'" + nome + "\' \u00e9 obrigat\u00f3rio");
			erro = 0;
		}
	}
	if (input.value != ""){
		if (vmin != ''){
  			if (input.value.length < vmin){
    			alert("Digite no m\u00ednimo " + vmin + " caracteres no campo \'" + nome + "\'");
				erro = 0;
			}
  		}
  		if (vmax != ''){
  			if (input.value.length > vmax){
    			alert("Digite no m\u00e1ximo " + vmax + " caracteres no campo \'" + nome + "\'");
				erro = 0;
  			}
  		}
	}

	if (erro == 0){
		input.focus();
		return (false);
	}
	else{
		return (true);
	}
}
//-->